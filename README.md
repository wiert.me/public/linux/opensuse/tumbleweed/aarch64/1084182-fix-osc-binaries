# Fixed OSC binaries for [bug 1084812]((https://bugzilla.opensuse.org/show_bug.cgi?id=1084812)) on aarch64

The binaries are in the subdirectory `binaries` for revisions `2.27-485.3`, `2.27-489.7` and `2.27-491.4`.

For now it helps getting a system back "working" but I've not fully tested a subsequent `zypper dist-upgrade` as [that failed on one system](https://gist.github.com/jpluimers/edf5a679621e6e68af936715430fc8bd).

I created this repository because I got stuck at
[Tumbleweed on Raspberry Pi 3 – Bug 1084419 – Glibc update to 2.27 causes segfault during name resolution](https://wiert.me/2018/03/15/tumbleweed-on-raspberry-pi-3-bug-1084419-glibc-update-to-2-27-causes-segfault-during-name-resolution/) and
when replying to https://forums.opensuse.org/showthread.php/530303-My-zypper-is-stuck-zypper-segfaults-in-TW-4-15-7-1-default-on-Raspberry-Pi-aarch64 the forum system complained I had too many images in my post because the same forum system - without warning - replaced quite a bit of text with smileys, then when trying to re-post the corrected message, expired the session, came up with a logon-dialog, and forgot about the one-hour editing I did on that post.

Thanks though to [malcolmlewis](https://forums.opensuse.org/member.php/740-malcolmlewis) though for getting me started on this. He also provided [the initial steps](https://bugzilla.opensuse.org/show_bug.cgi?id=1084812#c5) that - due to lack of formatting - were a bit too cryptic for me to get in one time.

## Introduction

A while ago, on [openSUSE Tumbleweed](https://en.wikipedia.org/wiki/OpenSUSE) on [arm64](https://en.wikipedia.org/wiki/arm64) (also called [aarch64](https://en.wikipedia.org/wiki/aarch64)), [DNS](https://en.wikipedia.org/wiki/Domain_Name_System#DNS_resolvers) lookup of [IPv4](https://en.wikipedia.org/wiki/IPv4) addresses through [glibc](https://en.wikipedia.org/wiki/GNU_C_Library) was broken: [Bug 1084812 - [aarch64] IPv4 DNS leading to segfaults](https://bugzilla.opensuse.org/show_bug.cgi?id=1084812).

This affects for instance [Raspberry Pi 3](https://en.wikipedia.org/wiki/Raspberry_Pi_3) users that installed Tumbleweed before that date and performed a `zypper dist-upgrade` after that date.

The effect is that IPv4 addresses cannot be looked up through glibc any more, which is a big problem as most of the internet is still IPv4 based, including the [`zypper`](https://en.wikipedia.org/wiki/ZYpp) update locations.

The [openQA](https://openqa.opensuse.org) build breaking it was [Overall Summary of openSUSE Tumbleweed AArch64 build 20180305](https://openqa.opensuse.org/tests/overview?distri=opensuse&version=Tumbleweed&build=20180305&groupid=3) [[Archived](https://archive.is/jV2tU)] and it might be fixed in [Overall Summary of openSUSE Tumbleweed AArch64 build 20180322](https://openqa.opensuse.org/tests/overview?distri=opensuse&version=Tumbleweed&build=20180322&groupid=3) [[Archived](https://archive.is/67nhe)]

> You can always see the latest aarch64 build for Tumbleweed at <https://openqa.opensuse.org/group_overview/3>

There are temporary fixes for [Bug 1084812](https://bugzilla.opensuse.org/show_bug.cgi?id=1084812) from https://build.opensuse.org/package/binaries/home:StefanBruens:branches:openSUSE:Factory:ARM/glibc/openSUSE_Factory_ARM which are in this repository.

This repository contains binary fixes downloaded through OSC (opensuse Build Service)

This readme document continues with these sctions:

- [Comparing your local version with the on-line versions](#comparing-your-local-version-with-the-on-line-versions)
- [Downloading the fixes on a working system](#downloading-the-fixes-on-a-working-system)
- [Applying the fixes on a broken system](#applying-the-fixes-on-a-broken-system)
- [After installing the patches: upgrading with zypper](#after-installing-the-patches-upgrading-with-zypper)
- [Observations](#observations)

## [Comparing your local version with the on-line versions](https://wiert.me/2018/04/05/tumbleweed-comparing-your-local-version-with-the-on-line-versions/)

Before upgrading a Tumbleweed system, it makes sense to check which is your local and which is the on-line version. This is actually a tad more complicated than it sounds.

There are three versions involved:

- the local release version on your system
- the on-line release version from for instance `aarch64` used on <https://download.opensuse.org/ports/aarch64/tumbleweed/repo/oss/repodata/> file `*-primary.xml.gz` and <http://download.opensuse.org/ports/aarch64/tumbleweed/repo/oss/aarch64/> in files `openSUSE-release-*-1.1.aarch64.rpm`
- the on-line build version used on <https://openqa.opensuse.org/> and needs to be bound to the corresponding architecture in the <https://download.opensuse.org> tree

There is a mismatch between the last two as a side effect of decoupling the arm port a bit from the high checkin frequency of `openSUSE:Factory`; ARM simply has not enough power to build the snapshot in the same time Intel and PowerPC can do.

[Dominique a.k.a. DimStar (Dim*) – A passionate openSUSE user](http://dominique.leuenberger.net/) thinks the last two are mismatched is a side effect off [osc service remoterun operates on outdated sources (product builder) · Issue #4768 · openSUSE/open-build-service · GitHub](https://github.com/openSUSE/open-build-service/issues/4768).

He also tech-reviewed this post.

### Your local release version

There are various ways to get your local version:

The easiest is to inspect the file `/etc/os-release`, for instance `20180208` in the file content:

``` text
NAME="openSUSE Tumbleweed"
# VERSION="20180208 "
ID=opensuse
ID_LIKE="suse"
VERSION_ID="20180208"
PRETTY_NAME="openSUSE Tumbleweed"
ANSI_COLOR="0;32"
CPE_NAME="cpe:/o:opensuse:tumbleweed:20180208"
BUG_REPORT_URL="https://bugs.opensuse.org"
HOME_URL="https://www.opensuse.org/"
```

You can also perform `rpm --query --provides openSUSE-release | grep "product(openSUSE)"` which for the same install returned this `product(openSUSE) = 20180208-0`.

Finally, you can use `zypper` to query the installed product which also includes the version:

``` text
$ zypper search --installed-only --type product --details
Loading repository data...
Reading installed packages...

S  | Name     | Type    | Version    | Arch    | Repository       
---+----------+---------+------------+---------+------------------
i+ | openSUSE | product | 20180228-0 | aarch64 | (System Packages)
```

### The on-line release version

I will explain this for the `aarch64` architecture, but the mechanism holds for all architectures, it is just that the directory names vary.

Architectures and base directories you can use this mechanism with:

- <https://download.opensuse.org/tumbleweed/repo/oss/repodata/> for `i586`, `i686` and `x86_64`
  - <https://download.opensuse.org/tumbleweed/repo/oss/i586/?P=openSUSE-release-2*>
  - <https://download.opensuse.org/tumbleweed/repo/oss/i686/?P=openSUSE-release-2*> -> no version: contains only kernels
  - <https://download.opensuse.org/tumbleweed/repo/oss/x86_64/?P=openSUSE-release-2*>
- <https://download.opensuse.org/ports/aarch64/tumbleweed/repo/oss/repodata/> for `aarch64`
  - <https://download.opensuse.org/ports/aarch64/tumbleweed/repo/oss/aarch64/?P=openSUSE-release-2*>
- <https://download.opensuse.org/ports/armv6hl/tumbleweed/repo/oss/suse/repodata/> for `armv6hl`
  - <https://download.opensuse.org/ports/armv6hl/tumbleweed/repo/oss/suse/armv6hl/?P=openSUSE-release-2*>
- <https://download.opensuse.org/ports/armv7hl/tumbleweed/repo/oss/suse/repodata/> for `armv7hl`
  - <https://download.opensuse.org/ports/armv7hl/tumbleweed/repo/oss/suse/armv7hl/?P=openSUSE-release-2*>
- <https://download.opensuse.org/ports/ppc/tumbleweed/repo/oss/repodata/> for `ppc`, `ppc64` and `ppc64le`
  - <https://download.opensuse.org/ports/ppc/tumbleweed/repo/oss/ppc/?P=openSUSE-release-2*> -> unsupported PowerMac platform
  - <https://download.opensuse.org/ports/ppc/tumbleweed/repo/oss/ppc64/?P=openSUSE-release-2*>
  - <https://download.opensuse.org/ports/ppc/tumbleweed/repo/oss/ppc64le/?P=openSUSE-release-2*>
- <https://download.opensuse.org/ports/zsystems/tumbleweed/repo/oss/suse/repodata/> for `s390x`
  - <https://download.opensuse.org/ports/zsystems/tumbleweed/repo/oss/suse/s390x/?P=openSUSE-release-2*> 

Each architecture contains the version number in two kinds of places:

1. The content of the repository meta data in a file named `*-primary.xml.gz` referenced from `repomd.xml` in the `repodata` subdirectory
2. The filename of a package named `?P=openSUSE-release-2*`

Back to the `aarch64` architecture:

- Getting the version through `repomd.xml` is what `zypper` does based on a slight adoption of <https://en.opensuse.org/openSUSE:Standards_Rpm_Metadata#Repository_layout> resulting in release version `20180324`:

  1. Inspect <https://download.opensuse.org/ports/aarch64/tumbleweed/repo/oss/repodata/repomd.xml> for the name of `*-primary.xml.gz` 
     (in this case <https://download.opensuse.org/ports/aarch64/tumbleweed/repo/oss/repodata/d701c298b21d0b995c9560f9cfcc84685cb916deacc4f4c4a613a9b9d8f5aa57-primary.xml.gz>
  2. Download that `.gz` file and uncompress it
  3. Inspect the `*-primary.xml` from it, look inside the `metatadata` root element for a `package` having a `name` element with value `openSUSE-release`: 
     that `package` element now has a `version` element having a `ver` attribute containing the release version text.

     Example file content:

     ``` xml
     <?xml version="1.0" encoding="UTF-8"?>
     <metadata xmlns="http://linux.duke.edu/metadata/common" xmlns:rpm="http://linux.duke.edu/metadata/rpm" packages="35006">
     <!-- ... -->
     <package type="rpm">
       <name>openSUSE-release</name>
       <arch>aarch64</arch>
       <version epoch="0" ver="20180324" rel="1.1"/>
       <!-- ... -->
     </package>
     <!-- ... -->
     </metadata>
     ```

- Getting the version through the name of the `openSUSE-release-2*` package:

  1. Search for a matching filename at <https://download.opensuse.org/ports/aarch64/tumbleweed/repo/oss/aarch64/?P=openSUSE-release-2*>
  2. The list contains `openSUSE-release-20180324-1.1.aarch64.rpm` indicating release version `20180324`

### The on-line build version

I will explain this for the `aarch64` architecture, but the mechanism holds for all architectures that build on openQA, it is just that the directory names vary and not all architectures are running on openQA.

Architectures and base directories you can use this mechanism with:

- <https://download.opensuse.org/tumbleweed/repo/oss/media.1/> for `i586`, `i686` and `x86_64` (let's call this platform `x86_64` as it is not Intel specific, in fact foriginated at AMD)
  - <https://download.opensuse.org/tumbleweed/repo/oss/media.1/media>
  - <https://download.opensuse.org/tumbleweed/repo/oss/media.1/products>
  - <https://openqa.opensuse.org/group_overview/1> openSUSE Tumbleweed (covering `x86_64` and some `i586`)
  - <https://openqa.opensuse.org/group_overview/32> openSUSE Tumbleweed Kernel (covering `x86_64`)
- <https://download.opensuse.org/ports/aarch64/tumbleweed/repo/oss/media.1/> for `aarch64` (let's call this platform [ARM](https://en.wikipedia.org/wiki/ARM_architecture))
  - <https://download.opensuse.org/ports/aarch64/tumbleweed/repo/oss/media.1/media>
  - <https://download.opensuse.org/ports/aarch64/tumbleweed/repo/oss/media.1/products>
  - <https://openqa.opensuse.org/group_overview/3> openSUSE Tumbleweed AArch64 (covering `aarch64`)
- <https://download.opensuse.org/ports/ppc/tumbleweed/repo/oss/media.1/> for `ppc`, `ppc64` and `ppc64le` (let's call this platform [PowerPC](https://en.wikipedia.org/wiki/PowerPC))
  - <https://download.opensuse.org/ports/ppc/tumbleweed/repo/oss/media.1/media>
  - <https://download.opensuse.org/ports/ppc/tumbleweed/repo/oss/media.1/products>
  - <https://openqa.opensuse.org/group_overview/4> openSUSE Tumbleweed PowerPC (covering both `ppc64` and `ppc64le`)
- <https://download.opensuse.org/ports/zsystems/tumbleweed/repo/oss/media.1/> for `s390x` (let's call this platform [System Z](https://en.wikipedia.org/wiki/System_Z))
  - <https://download.opensuse.org/ports/zsystems/tumbleweed/repo/oss/media.1/media>
  - <https://download.opensuse.org/ports/zsystems/tumbleweed/repo/oss/media.1/products>
  - <https://openqa.opensuse.org/group_overview/34> openSUSE Tumbleweed s390x

Architectures not on openQA:

- `armv6hl`
- `armv7hl`

Each platform  contains the version number in two kinds of places:

1. The content of the repository meta data in the file named `media.1/media`  and `media.1/products`
2. Names used in the openQA links

Back to the `aarch64` architecture on the ARM platform:

- <https://download.opensuse.org/ports/aarch64/tumbleweed/repo/oss/media.1/media>

  ``` text
  openSUSE - openSUSE-20180322-aarch64-Build46.1-Media
  openSUSE-20180322-aarch64-Build46.1
  1
  ```

- <https://download.opensuse.org/ports/aarch64/tumbleweed/repo/oss/media.1/products>

  ``` text
  / openSUSE 20180322-0
  ```    

- <https://openqa.opensuse.org/group_overview/3> openSUSE Tumbleweed AArch64

  ``` text
  ...
  Build20180322 (10 days ago) testing
  ...
  ```

  - The above text points to [Overall Summary of openSUSE Tumbleweed AArch64 build 20180322](https://openqa.opensuse.org/tests/overview?distri=opensuse&version=Tumbleweed&build=20180322&groupid=3)

## Downloading the fixes on a working system

You need a working system for this, that has a relatively recent Tumbleweed install (see "notes on step 2" below), but it does not have to be the same processor architecture.

### The one-time steps first

1. You need an [OSC](https://en.opensuse.org/openSUSE:OSC) (Open Build System, not to be confused openSUSE build system), which you can get by going <https://build.opensuse.org/>, then clicking [Sign Up](https://secure-www.novell.com/selfreg/jsp/createOpenSuseAccount.jsp?%22).
2. You need to have `osc` installed in your download machine; install it with the  installation steps below.

   ```bash
   sudo zypper refresh
   sudo zypper install osc
   ```

3. (optional) You need to logon to `osc` by executing `osc token` and entering your username and password. This will save credentials in `~/.config/osc/oscrc` (stored with `chmod` rights `0600`).
   Example output:

   ```text
   $ osc token

   Your user account / password are not configured yet.
   You will be asked for them below, and they will be stored in
   /home/jeroenp/.config/osc/oscrc for future use.

   Creating osc configuration file /home/jeroenp/.config/osc/oscrc ...
   Username: ..........
   Password: ..........
   done
   <directory count="0"/>
   ```

> #### Notes on step 2
>
> If you forget the refresh, you can get a message like this:
>
> ``` text
> File './suse/noarch/boost-license1_66_0-1.66.0-2.4.noarch.rpm' not found on medium 'https://download.opensuse.org/ports/aarch64/tumbleweed/repo/oss/'
> ```
>
> If your system is not recent enough, then you get something like the below version mismatch after installing osc:
>
> ``` text
> $ osc token
> Traceback (most recent call last):
>   File "/usr/bin/osc", line 10, in <module>
>     from osc import commandline, babysitter
>   File "/usr/lib/python2.7/site-packages/osc/babysitter.py", line 14, in <module>
>     from urlgrabber.grabber import URLGrabError
>   File "/usr/lib/python2.7/site-packages/urlgrabber/__init__.py", line 53, in <module>
>     from grabber import urlgrab, urlopen, urlread
>   File "/usr/lib/python2.7/site-packages/urlgrabber/grabber.py", line 427, in <module>
>     import pycurl
> ImportError: pycurl: libcurl link-time version (7.55.0) is older than compile-time version (7.57.0)
> $ rpm --query --all | grep libcurl
> libcurl4-7.55.0-1.1.aarch64
> ```

### Any time you want to download the most recent binaries:

1. Change to the directory where you want to store them
2. Execute `osc getbinaries home:StefanBruens:branches:openSUSE:Factory:ARM openSUSE_Factory_ARM aarch64`

The first time you execute the `osc` command, it will create a subdirectory `binaries`, then fill it with the most recently built binaries.

Subsequent times you execute `osc` will update the `binaries` directory with the most recently built binaries.

Example run2:

``` text
$ osc getbinaries home:StefanBruens:branches:openSUSE:Factory:ARM openSUSE_Factory_ARM aarch64
Creating binaries
_buildenv                                                        100% |================================|  17 kB    00:00
_statistics                                                      100% |================================|  708 B    00:00
glibc-2.27-485.3.aarch64.rpm                                     100% |================================| 1.6 MB    00:00
glibc-64bit-2.27-485.3.aarch64_ilp32.rpm                         100% |================================| 1.3 MB    00:00
glibc-devel-2.27-485.3.aarch64.rpm                               100% |================================| 625 kB    00:00
glibc-devel-64bit-2.27-485.3.aarch64_ilp32.rpm                   100% |================================| 152 kB    00:00
glibc-devel-static-2.27-485.3.aarch64.rpm                        100% |================================|  13 MB    00:03
glibc-devel-static-64bit-2.27-485.3.aarch64_ilp32.rpm            100% |================================|  13 MB    00:03
glibc-extra-2.27-485.3.aarch64.rpm                               100% |================================| 112 kB    00:00
glibc-html-2.27-485.3.noarch.rpm                                 100% |================================| 999 kB    00:00
glibc-i18ndata-2.27-485.3.noarch.rpm                             100% |================================| 3.7 MB    00:01
glibc-info-2.27-485.3.noarch.rpm                                 100% |================================| 1.0 MB    00:00
glibc-locale-2.27-485.3.aarch64.rpm                              100% |================================| 7.1 MB    00:01
glibc-locale-64bit-2.27-485.3.aarch64_ilp32.rpm                  100% |================================| 1.3 MB    00:00
glibc-profile-2.27-485.3.aarch64.rpm                             100% |================================|  13 MB    00:03
glibc-profile-64bit-2.27-485.3.aarch64_ilp32.rpm                 100% |================================|  13 MB    00:03
nscd-2.27-485.3.aarch64.rpm                                      100% |================================| 165 kB    00:00
rpmlint.log                                                      100% |================================| 2.2 kB    00:00
```

``` text
$ osc getbinaries home:StefanBruens:branches:openSUSE:Factory:ARM openSUSE_Factory_ARM aarch64
_buildenv                                                        100% |================================|  17 kB    00:00
_statistics                                                      100% |================================|  708 B    00:00
glibc-2.27-489.7.aarch64.rpm                                     100% |================================| 1.6 MB    00:00
glibc-64bit-2.27-489.7.aarch64_ilp32.rpm                         100% |================================| 1.3 MB    00:00
glibc-devel-2.27-489.7.aarch64.rpm                               100% |================================| 625 kB    00:00
glibc-devel-64bit-2.27-489.7.aarch64_ilp32.rpm                   100% |================================| 152 kB    00:00
glibc-devel-static-2.27-489.7.aarch64.rpm                        100% |================================|  13 MB    00:04
glibc-devel-static-64bit-2.27-489.7.aarch64_ilp32.rpm            100% |================================|  13 MB    00:04
glibc-extra-2.27-489.7.aarch64.rpm                               100% |================================| 112 kB    00:00
glibc-html-2.27-489.7.noarch.rpm                                 100% |================================| 999 kB    00:00
glibc-i18ndata-2.27-489.7.noarch.rpm                             100% |================================| 3.7 MB    00:01
glibc-info-2.27-489.7.noarch.rpm                                 100% |================================| 1.0 MB    00:00
glibc-locale-2.27-489.7.aarch64.rpm                              100% |================================| 7.0 MB    00:02
glibc-locale-64bit-2.27-489.7.aarch64_ilp32.rpm                  100% |================================| 1.3 MB    00:00
glibc-profile-2.27-489.7.aarch64.rpm                             100% |================================|  13 MB    00:04
glibc-profile-64bit-2.27-489.7.aarch64_ilp32.rpm                 100% |================================|  13 MB    00:03
nscd-2.27-489.7.aarch64.rpm                                      100% |================================| 165 kB    00:00
rpmlint.log                                                      100% |================================| 2.2 kB    00:00
no binaries found: Either the package xf86-video-freedreno does not exist or no binaries have been built.
```

### `osc` installation steps



## Applying the fixes on a broken system

### One time steps

1. Ensure that `git` can resolve `gitlab.com` over IPv4:

   ``` bash
   sudo sh -c 'echo "52.167.219.168 gitlab.com" >> /etc/hosts'
   ```

2. Ensuring zypper can reach the update server over IPv4:

   ``` bash
   sudo sh -c 'echo "195.135.221.134 download.opensuse.org" >> /etc/hosts'
   ```

### Steps for each commit you want to try

1. Go to a temporary directory
2. Try the git way:
   2. Clone this repository: `git clone https://gitlab.com/wiert.me/public/linux/opensuse/tumbleweed/aarch64/1084182-fix-osc-binaries.git`
   3. Change to `binaries` subdirectory in the repository: `cd 1084182-fix-osc-binaries/binaries`
   4. (Optional) checkout a revision you want
       1. View the revisions with `git log`
       2. From the commit you want to try, note the base64 hash (for instance `2b2d40b0c8070d3731357a885c1465e4253acbf7`)
       3. Check out that commit (for instance `git checkout 2b2d40b0c8070d3731357a885c1465e4253acbf7`)
3. If the git way failed on the target system then:
   1. Try the git way on a different system
   2. Transfer the files over using ssh (for instance using [FileZilla](https://en.wikipedia.org/wiki/FileZilla)) 

4. Find which of the `glibc` or `nsdc` packages are installed now, in my case:

   ``` text
   $ rpm --query --all | grep "\(glibc-\|nscd\)" | sort
   glibc-2.27-2.1.aarch64
   glibc-extra-2.27-2.1.aarch64
   glibc-locale-2.27-2.1.aarch64
   ```

   or:

   ```
   $ rpm --query --all | grep "\(glibc-\|nscd\)" | sort
   glibc-2.27-1.1.aarch64
   glibc-extra-2.27-1.1.aarch64
   glibc-locale-2.27-1.1.aarch64
   nscd-2.27-1.1.aarch64
   ```

5. Switch to user `root`, for instance with `sudo -i`
6. Install the packages you need (see the outcome of step 5), replacing the version number in this command, confirming any vendor changes and ignoreing any signatures:

   ``` bash
   zypper install --no-refresh glibc-2.27-489.7.aarch64.rpm glibc-extra-2.27-489.7.aarch64.rpm glibc-locale-2.27-489.7.aarch64.rpm
   ```

   or:

   ``` bash
   zypper install --no-refresh glibc-2.27-489.7.aarch64.rpm glibc-extra-2.27-489.7.aarch64.rpm glibc-locale-2.27-489.7.aarch64.rpm nscd-2.27-489.7.aarch64.rpm
   ```

Especially important here is the `--no-refresh` option: without it, it will at least contact `download.opensuse.org` and - by design - any host [the mirror-infrastructure](https://en.opensuse.org/openSUSE:Mirrors) redirects do. Those will have their IPv4 address looked up by DNS crash, and `zypper` has no `--verbose` level showing the redirects. From tha [zypper man page](https://en.opensuse.org/SDB:Zypper_manual_(plain)):

> ```
> --no-refresh
>            Do not auto-refresh repositories (ignore the auto-refresh setting). Useful to save time when doing
>            operations like search, if there is not a need to have a completely up to date metadata.
> ```

More on the mirror infrastructure:

- [openSUSE Download Mirrors - Overview](https://mirrors.opensuse.org/)
- [openSUSE:Mirrors](https://en.opensuse.org/openSUSE:Mirrors)
- [MirrorBrain](https://web.archive.org/web/20180408080013/https://en.opensuse.org/MirrorBrain)
- [mirrorbrain    a download redirector and torrent/metalink generator](http://mirrorbrain.org/)
- [mirrorbrain documentation](http://mirrorbrain.org/docs/)

   Example output:

   ```
   # zypper install glibc-2.27-489.7.aarch64.rpm glibc-extra-2.27-489.7.aarch64.rpm glibc-locale-2.27-489.7.aarch64.rpm
   Loading repository data...
   Reading installed packages...
   Resolving package dependencies...

   The following 3 packages are going to be upgraded:
   glibc glibc-extra glibc-locale

   The following 3 packages are going to change vendor:
   glibc         openSUSE -> obs://build.opensuse.org/home:StefanBruens
   glibc-extra   openSUSE -> obs://build.opensuse.org/home:StefanBruens
   glibc-locale  openSUSE -> obs://build.opensuse.org/home:StefanBruens

   3 packages to upgrade, 3  to change vendor.
   Overall download size: 8.7 MiB. Already cached: 0 B. After the operation, additional 344.0 B will be used.
   Continue? [y/n/...? shows all options] (y): y
   Retrieving package glibc-2.27-489.7.aarch64                                                 (1/3),   1.6 MiB (  6.3 MiB unpacked)
   glibc-2.27-489.7.aarch64.rpm:
       Header V3 RSA/SHA256 Signature, key ID 7b4595dd: NOKEY

   glibc-2.27-489.7.aarch64 (Plain RPM files cache): Signature verification failed [4-Signatures public key is not available]
   Abort, retry, ignore? [a/r/i] (a): i
   Retrieving package glibc-extra-2.27-489.7.aarch64                                           (2/3), 112.0 KiB ( 71.8 KiB unpacked)
   glibc-extra-2.27-489.7.aarch64.rpm:
       Header V3 RSA/SHA256 Signature, key ID 7b4595dd: NOKEY

   glibc-extra-2.27-489.7.aarch64 (Plain RPM files cache): Signature verification failed [4-Signatures public key is not available]
   Abort, retry, ignore? [a/r/i] (a): i
   Retrieving package glibc-locale-2.27-489.7.aarch64                                          (3/3),   7.0 MiB (147.1 MiB unpacked)
   glibc-locale-2.27-489.7.aarch64.rpm:
       Header V3 RSA/SHA256 Signature, key ID 7b4595dd: NOKEY

   glibc-locale-2.27-489.7.aarch64 (Plain RPM files cache): Signature verification failed [4-Signatures public key is not available]
   Abort, retry, ignore? [a/r/i] (a): i
   nscd-2.27-489.7.aarch64.rpm                                               100% |================================| 165 kB    00:00
   (1/3) Installing: glibc-2.27-489.7.aarch64 ................................................................................[done]
   Additional rpm output:
   warning: /var/cache/zypper/RPMS/glibc-2.27-489.7.aarch64.rpm: Header V3 RSA/SHA256 Signature, key ID 7b4595dd: NOKEY


   (2/3) Installing: glibc-extra-2.27-489.7.aarch64 ..........................................................................[done]
   Additional rpm output:
   warning: /var/cache/zypper/RPMS/glibc-extra-2.27-489.7.aarch64.rpm: Header V3 RSA/SHA256 Signature, key ID 7b4595dd: NOKEY


   (3/3) Installing: glibc-locale-2.27-489.7.aarch64 .........................................................................[done]
   Additional rpm output:
   warning: /var/cache/zypper/RPMS/glibc-locale-2.27-489.7.aarch64.rpm: Header V3 RSA/SHA256 Signature, key ID 7b4595dd: NOKEY


   There are some running programs that might use files deleted by recent upgrade. You may wish to check and restart some of them. Run 'zypper ps -s' to list these programs.
   ```

## After installing the patches: upgrading with zypper

After the patches you have to keep the "obsolete" patches using `zypper dist-upgrade --no-allow-vendor-change` and choosing `2` for each patched package:

``` text
# zypper dist-upgrade --no-allow-vendor-change
Repository 'openSUSE-Ports-Tumbleweed-repo-oss' is up to date.
All repositories have been refreshed.
Loading repository data...
Reading installed packages...
Computing distribution upgrade...
3 Problems:
Problem: problem with installed package glibc-2.27-489.7.aarch64
Problem: problem with installed package glibc-extra-2.27-489.7.aarch64
Problem: problem with installed package glibc-locale-2.27-489.7.aarch64

Problem: problem with installed package glibc-2.27-489.7.aarch64
 Solution 1: install glibc-2.27-2.1.aarch64 (with vendor change)
  obs://build.opensuse.org/home:StefanBruens  -->  openSUSE
 Solution 2: keep obsolete glibc-2.27-489.7.aarch64

Choose from above solutions by number or skip, retry or cancel [1/2/s/r/c] (c): 2

Problem: problem with installed package glibc-extra-2.27-489.7.aarch64
 Solution 1: install glibc-extra-2.27-2.1.aarch64 (with vendor change)
  obs://build.opensuse.org/home:StefanBruens  -->  openSUSE
 Solution 2: keep obsolete glibc-extra-2.27-489.7.aarch64

Choose from above solutions by number or skip, retry or cancel [1/2/s/r/c] (c): 2

Problem: problem with installed package glibc-locale-2.27-489.7.aarch64
 Solution 1: install glibc-locale-2.27-2.1.aarch64 (with vendor change)
  obs://build.opensuse.org/home:StefanBruens  -->  openSUSE
 Solution 2: keep obsolete glibc-locale-2.27-489.7.aarch64

Choose from above solutions by number or skip, retry or cancel [1/2/s/r/c] (c): 2
Resolving dependencies...
Computing distribution upgrade...

The following 3 items are locked and will not be changed by any action:
...
```

## Observations

Broken systems:

``` text
> rpm -qa | grep glibc | sort
glibc-2.27-1.1.aarch64
glibc-extra-2.27-1.1.aarch64
glibc-locale-2.27-1.1.aarch64
```

``` text
$ rpm -qa | grep glibc | sort
glibc-2.27-2.1.aarch64
glibc-extra-2.27-2.1.aarch64
glibc-locale-2.27-2.1.aarch64
```
